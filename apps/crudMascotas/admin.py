from django.contrib import admin
from .models import Mascota, Estado

admin.site.register(Mascota)
admin.site.register(Estado)