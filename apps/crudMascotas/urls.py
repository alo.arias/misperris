from django.conf.urls import url
from apps.crudMascotas.views import MascotaCreate, MascotaList, MascotaDelete, MascotaUpdate, MascotaShow, search

app_name="crudMascotas"

urlpatterns = [
    url(r'^nuevo/', MascotaCreate.as_view(), name='mascota_crear'),
    url(r'^listar', MascotaList.as_view(), name='mascota_listar'),
    url(r'^eliminar/(?P<pk>\d+)/$', MascotaDelete.as_view(), name='mascota_eliminar'),
    url(r'^editar/(?P<pk>\d+)/$', MascotaUpdate.as_view(), name='mascota_editar'),
    url(r'^mostrar/(?P<pk>\d+)/$', MascotaShow.as_view(), name='mascota_mostrar'),
    url(r'^buscar/$', search, name='mascota_buscar'),
]