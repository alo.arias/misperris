# Generated by Django 2.1.2 on 2018-10-27 04:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Estado',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=40)),
            ],
        ),
        migrations.CreateModel(
            name='Mascota',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=50)),
                ('razaPredominante', models.CharField(max_length=50)),
                ('descripcion', models.CharField(max_length=50)),
                ('foto', models.ImageField(blank=True, upload_to='img_perritos/')),
                ('estado', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='crudMascotas.Estado')),
            ],
        ),
    ]
