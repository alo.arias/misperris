import django_filters
from apps.crudMascotas.models import Mascota, Estado

class MascotaFilter(django_filters.FilterSet):
	class Meta:
		model = Mascota
		fields = ['nombre', 'razaPredominante', 'estado',]