from django import forms
from apps.crudMascotas.models import Mascota, Estado

class MascotaForm(forms.ModelForm):
	class Meta:
		model = Mascota
		fields = [
			'id',
			'nombre',
			'razaPredominante',
			'descripcion',
			'estado',
			'foto',
		]

		labels = {
			'id': "Identificacion",
			'nombre': "Nombre Mascota",
			'razaPredominante': "Raza Predominante",
			'descripcion': "Descripcion",
			'estado': "Estado",
			'foto': "Foto"
		}

		widgets = {
			'id': forms.TextInput(attrs={'class':'form-control'}),
			'nombre': forms.TextInput(attrs={'class':'form-control'}),
			'razaPredominante': forms.TextInput(attrs={'class':'form-control'}),
            'descripcion': forms.TextInput(attrs={'class':'form-control'}),
			'estado': forms.Select(attrs={'class':'form-control', 'required': 'true'}),
		}