from django.db import models

# Create your models here.
class Estado(models.Model):
	id = models.IntegerField(primary_key=True)
	nombre = models.CharField(max_length=40)

	def __str__(self):
		return '{}'.format(self.nombre)

class Mascota(models.Model):
	id = models.IntegerField(primary_key=True)
	nombre = models.CharField(max_length=50)
	razaPredominante = models.CharField(max_length=50)
	descripcion = models.CharField(max_length=50)
	estado = models.ForeignKey(Estado, null=True, blank=True, on_delete=models.CASCADE)
	foto = models.ImageField(blank=True, null=True, upload_to='img_perritos/', default = 'img_perritos/None/No_Image_Available.jpg') #agregar default

	def __str__(self):
		return '{}'.format(self.nombre)