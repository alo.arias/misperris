from django.shortcuts import render
from django.views.generic import CreateView, ListView, DeleteView, UpdateView, DetailView
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin

from apps.crudMascotas.models import Mascota, Estado
from apps.crudMascotas.forms import MascotaForm
from apps.crudMascotas.filters import MascotaFilter

# Create your views here.


class MascotaCreate(LoginRequiredMixin, CreateView):
	model = Mascota
	form_class = MascotaForm
	template_name = 'crudMascotas/mascota_form.html'
	success_url = reverse_lazy('mascotas:mascota_listar')


class MascotaList(LoginRequiredMixin, ListView):
	queryset = Mascota.objects.order_by('id')
	template_name = 'crudMascotas/mascota_list.html'
	paginate_by = 5

class MascotaUpdate(LoginRequiredMixin, UpdateView):
	model = Mascota
	form_class = MascotaForm
	template_name = 'crudMascotas/mascota_form.html'
	success_url = reverse_lazy('mascotas:mascota_listar')

class MascotaDelete(LoginRequiredMixin, DeleteView):
	model = Mascota
	template_name = 'crudMascotas/mascota_delete.html'
	success_url = reverse_lazy('mascotas:mascota_listar')

class MascotaShow(LoginRequiredMixin, DetailView):
	model = Mascota
	template_name = 'crudMascotas/mascota_show.html'

def search(request):
	mascota_list = Mascota.objects.all()
	mascota_filter = MascotaFilter(request.GET, queryset=mascota_list)
	return render(request, 'crudMascotas/mascota_list2.html', {'filter': mascota_filter})
	