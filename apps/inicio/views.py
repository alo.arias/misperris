from django.shortcuts import redirect, render, HttpResponseRedirect
from .models import Contacto
from apps.inicio.forms import registro


def home(request):
    return render(request, 'inicio/index.html')

def formulario(request):
    if request.method == 'POST':
        form = registro(request.POST)

        if form.is_valid():
            form.save()
        else:
            print(form.errors)
        return render(request, 'inicio/contacto.html', {})
    else:
        form = registro() 
        return render(request, 'inicio/contacto.html', {'form':form})  