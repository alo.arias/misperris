from django.db import models


class Contacto(models.Model):
    rut = models.CharField(max_length=50, unique = True)
    nombre = models.CharField(max_length=50)
    fecha = models.DateField()
    telefono = models.IntegerField()
    correo = models.CharField(max_length=54)
    region = models.CharField(max_length=50)
    comuna = models.CharField(max_length=50)
    tipoHogar = models.CharField(max_length=50)

    def __str__(self):
        return self.rut


    class Meta:
        verbose_name = "Contacto"
        verbose_name_plural = "Contactos"