from django.urls import path
from django.conf.urls import url
from . import views

app_name="inicio"

urlpatterns = [
    #path('iniciio/$', views.home, name = "home"),
    url(r'^$', views.home, name="home"),
    url(r'^formulario/$', views.formulario, name="form"),
]