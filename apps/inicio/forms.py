from django import forms
from .models import Contacto

class registro(forms.ModelForm):
    class Meta:
        model = Contacto
        fields = ('rut', 'nombre', 'fecha', 'telefono', 'correo', 'region', 'comuna', 'tipoHogar')